package com.liquibase.demo.service;

import com.liquibase.demo.domain.Student;
import com.liquibase.demo.repository.StudentRepository;
import net.bytebuddy.pool.TypePool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getAllStudents(){
        List<Student> studentList = new ArrayList<>();
        try{
            studentRepository.findAll().forEach(student -> studentList.add(student));
        } catch (IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }

        return studentList;
    }
}
