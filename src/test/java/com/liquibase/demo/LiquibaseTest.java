package com.liquibase.demo;

import com.liquibase.demo.domain.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import static org.assertj.core.api.Assertions.assertThat;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;




@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LiquibaseTest {


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /*
    @Container
    public static MariaDBContainer mariaDBContainer = new MariaDBContainer();
     */
    @Container
    public static MySQLContainer mySQLContainer = new MySQLContainer();

    /*
    @DynamicPropertySource
    static void mariadbProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mariaDBContainer::getJdbcUrl);
        registry.add("spring.datasource.password", mariaDBContainer::getPassword);
        registry.add("spring.datasource.username", mariaDBContainer::getUsername);
    }
     */

    @DynamicPropertySource
    static void mySQLProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mySQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", mySQLContainer::getPassword);
        registry.add("spring.datasource.username", mySQLContainer::getUsername);
    }


    @Sql("classpath:sql/insert_student.sql")
    @Test
    public void someTestMethod() throws Exception {

       // studentRepository.save(new Student(5,"killua","Hunter_Class","test2 ok","test3 ok"));
        final Student EXPECTED = new Student(1, "killua", "Hunter_Class", "test2 ok",
                "test3 ok");

        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/student",
                String.class)).contains("Killua");

    }
}
